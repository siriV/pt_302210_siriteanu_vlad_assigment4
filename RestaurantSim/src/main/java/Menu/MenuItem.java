package Menu;

import java.io.Serializable;

public interface MenuItem extends Serializable{
	
	public String getName();
	public void setName(String name);
	public Double getPrice();
	public boolean equals(Object obj);
	public int hashCode();
	public String toString();
}
