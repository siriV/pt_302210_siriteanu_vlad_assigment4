package Menu;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CompositeMenu implements MenuItem{
	
	private String name;
	private Set<BaseProduct> product;
	
	
	public CompositeMenu(String name, Set<BaseProduct> product)
	{
		this.name = name;
		this.product = product;
	}
	

	public String getName() 
	{
		return name;
	}


	public void setName(String name) 
	{
		this.name = name;
	}
	
	public Double getPrice()
	{
		Double price = 0.0;
		for(BaseProduct product : this.product)
		{
			price += product.getPrice();
		}
		
		return price;
	}
	
	public Set<BaseProduct> getProduct()
	{
		return this.product;
	}

	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		return result;
	}
	

	public boolean equals(Object obj) 
	{

		if(obj == null || !obj.getClass().getSimpleName().equals("CompositeMenu"))
		{
			return false;
		}
		
		CompositeMenu product = (CompositeMenu) obj;
		
		if(!product.getName().equals(this.getName()))
		{
			return false;
		}
		else
		{
			Set<BaseProduct> pr = product.getProduct();
			
			if(this.product.size() != pr.size())
			{
				return false;
			}
			for(BaseProduct item : this.product)
			{
				if(!pr.contains(item))
				{
					return false;
				}
			}
			
			return true;
		}
		
	}


	@Override
	public String toString() {
		String productString = "";
		for(BaseProduct product : this.product)
		{
			productString += product.getName() + " ";
		}
		return name + "-" + this.getPrice().toString() + "-" + productString;
	}
	

}
