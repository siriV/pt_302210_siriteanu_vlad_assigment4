package Menu;

import java.util.List;

public class BaseProduct implements MenuItem{
	
	
	private String name;
	private Double price;
	
	public BaseProduct(String name, Double price) 
	{
		this.name = name;
		this.price = price;
	}
	
	public String getName() 
	{
		return name;
	}
	

	public void setName(String name) 
	{
		this.name = name;
	}
	

	public Double getPrice() 
	{
		return price;
	}
	

	public void setPrice(Double price) 
	{
		this.price = price;
	}
	

	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
	public boolean equals(Object obj) 
	{
		
		if(obj == null || !obj.getClass().getSimpleName().equals("BaseProduct"))
		{
			return false;
		}
		
		BaseProduct product = (BaseProduct) obj;
		
		if(!product.getName().equals(this.getName()))
		{
			return false;
		}
		else if(!product.getPrice().equals(this.getPrice()))
		{
			return false;
		}
		return true;
		
	}

	@Override
	public String toString() {
		return  name + "-" + price + " ";
	}

	
}
