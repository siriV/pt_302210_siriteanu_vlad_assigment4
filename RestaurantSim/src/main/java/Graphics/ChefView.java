package Graphics;

import java.awt.Dimension;
import java.awt.Panel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import Data.RestaurantSerializator;
import Restaurant.Order;
import Restaurant.Restaurant;

public class ChefView extends JFrame implements Observer{
	
	private JLabel statusLabel = new JLabel("Status: ");
	private JTextField statusTextField = new JTextField();
	private Timer t;
	private Integer ok = 0;
	
	public ChefView()
	{
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Chef view");
		this.setLocation(0, 350);
		this.setSize(550, 100);
		this.setResizable(false);
		
		Panel chefPanel = new Panel();
		Panel displayPanel = new Panel();
		
		chefPanel.setLayout(new BoxLayout(chefPanel, BoxLayout.X_AXIS));
		chefPanel.add(statusLabel);
		chefPanel.add(Box.createRigidArea(new Dimension(5, 0)));
		chefPanel.add(statusTextField);
		statusTextField.setEditable(false);
		
		displayPanel.setLayout(new BoxLayout(displayPanel, BoxLayout.Y_AXIS));
		displayPanel.add(Box.createRigidArea(new Dimension(0, 30)));
		displayPanel.add(chefPanel);
		displayPanel.add(Box.createRigidArea(new Dimension(0, 30)));
		
		this.add(displayPanel);
	}

	public void update(Observable o, Object arg) {
		if(ok == 0)
		{
			ok = 1;
			t = new Timer();
			Order order = (Order) arg;
			t.schedule(new DisplayResult(), 5000);
			t.schedule(new ClearConsole(), 7000);
			this.statusTextField.setText("Cooking food for order " + order.getId());
		}
		
	}
	
	class DisplayResult extends TimerTask
	{	
		public void run()
		{
			statusTextField.setText("Food is ready!");
		}
	}
	
	class ClearConsole extends TimerTask
	{
		public void run()
		{
			statusTextField.setText("");
			ok = 0;
			t.cancel();
		}
	}

}
