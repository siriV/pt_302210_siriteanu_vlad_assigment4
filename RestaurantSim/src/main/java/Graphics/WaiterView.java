package Graphics;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.net.http.WebSocket.Listener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Restaurant.Order;

public class WaiterView extends JFrame{
	
	private JButton newOrder = new JButton("Create new order");
	private JButton addProduct = new JButton("Add product");
	private JButton removeProduct = new JButton("Remove product");
	private JButton sendOrder = new JButton("Send order");
	private JButton computeBill = new JButton("Compute bill");
	private JButton viewOrder = new JButton("View order");
	
	private JLabel orderId = new JLabel("Order id: ");
	private JLabel tableNumber = new JLabel("Table number: ");
	private JLabel product = new JLabel("Product name: ");
	private JLabel message = new JLabel("Info: ");
	
	private JTextField orderIdTextBox = new JTextField();
	private JTextField tableTextBox = new JTextField();
	private JTextField productTextBox = new JTextField();
	private JTextField messageTextBox = new JTextField();
	
	
	public WaiterView()
	{
		JPanel waiterPanel = new JPanel();
		JPanel idPanel = new JPanel();
		JPanel tablePanel = new JPanel();
		JPanel productPanel = new JPanel();
		JPanel messagePanel = new JPanel();
		JPanel operationPanel = new JPanel();
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Waiter view");
		this.setLocation(0, 0);
		this.setSize(750, 350);
		this.setResizable(false);
		
		idPanel.setLayout(new BoxLayout(idPanel, BoxLayout.X_AXIS));
		idPanel.add(Box.createRigidArea(new Dimension(30, 0)));
		idPanel.add(orderId);
		idPanel.add(Box.createRigidArea(new Dimension(10, 0)));
		idPanel.add(orderIdTextBox);
		
		tablePanel.setLayout(new BoxLayout(tablePanel, BoxLayout.X_AXIS));
		tablePanel.add(tableNumber);
		tablePanel.add(Box.createRigidArea(new Dimension(10, 0)));
		tablePanel.add(tableTextBox);

		productPanel.setLayout(new BoxLayout(productPanel, BoxLayout.X_AXIS));
		productPanel.add(product);
		productPanel.add(Box.createRigidArea(new Dimension(10, 0)));
		productPanel.add(productTextBox);
		
		messagePanel.setLayout(new BoxLayout(messagePanel, BoxLayout.X_AXIS));
		messagePanel.add(Box.createRigidArea(new Dimension(55, 0)));
		messagePanel.add(message);
		messagePanel.add(Box.createRigidArea(new Dimension(10, 0)));
		messagePanel.add(messageTextBox);
		messageTextBox.setEditable(false);
		
		operationPanel.setLayout(new BoxLayout(operationPanel, BoxLayout.X_AXIS));
		operationPanel.add(newOrder);
		operationPanel.add(Box.createRigidArea(new Dimension(5, 0)));
		operationPanel.add(addProduct);
		operationPanel.add(Box.createRigidArea(new Dimension(5, 0)));
		operationPanel.add(removeProduct);
		operationPanel.add(Box.createRigidArea(new Dimension(5, 0)));
		operationPanel.add(computeBill);
		operationPanel.add(Box.createRigidArea(new Dimension(5, 0)));
		operationPanel.add(viewOrder);
		operationPanel.add(Box.createRigidArea(new Dimension(5, 0)));
		operationPanel.add(sendOrder);
		operationPanel.add(Box.createRigidArea(new Dimension(5, 0)));
		
		waiterPanel.setLayout(new BoxLayout(waiterPanel, BoxLayout.Y_AXIS));
		waiterPanel.add(Box.createRigidArea(new Dimension(0, 80)));
		waiterPanel.add(idPanel);
		waiterPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		waiterPanel.add(tablePanel);
		waiterPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		waiterPanel.add(productPanel);
		waiterPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		waiterPanel.add(messagePanel);
		waiterPanel.add(Box.createRigidArea(new Dimension(0, 40)));
		waiterPanel.add(operationPanel);

		this.add(waiterPanel);
		
		
	}
	
	public Integer getOrderId()
	{
		try
		{
			Integer orderId = Integer.parseInt(this.orderIdTextBox.getText());
			
			return orderId;
		}
		catch(Exception ex)
		{
			return null;
		}
	}
	
	public Integer getOrderTable()
	{
		try
		{
			Integer orderTable = Integer.parseInt(this.tableTextBox.getText());
			
			return orderTable;
		}
		catch(Exception ex)
		{
			return null;
		}
	}
	
	public String getProduct()
	{
		return this.productTextBox.getText();
	}
	
	public void displayMessage(String message)
	{
		this.messageTextBox.setText(message);
	}
	
	void addNewOrderListener(ActionListener listen)
	{
		this.newOrder.addActionListener(listen);
	}
	
	void addProductListener(ActionListener listen)
	{
		this.addProduct.addActionListener(listen);
	}
	
	void removeProductListener(ActionListener listen)
	{
		this.removeProduct.addActionListener(listen);
	}
	
	void sendOrderListener(ActionListener listen)
	{
		this.sendOrder.addActionListener(listen);
	}
	
	void computeBillListener(ActionListener listen)
	{
		this.computeBill.addActionListener(listen);
	}
	
	void viewOrderListener(ActionListener listen)
	{
		this.viewOrder.addActionListener(listen);
	}

}
