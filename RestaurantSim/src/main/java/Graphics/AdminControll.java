package Graphics;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Menu.BaseProduct;
import Menu.CompositeMenu;
import Menu.MenuItem;
import Restaurant.Order;
import Restaurant.Restaurant;

public class AdminControll {
	
	private AdminView adminView;
	private Restaurant restaurant;
	
	public AdminControll(AdminView adminView, Restaurant restaurant)
	{
		this.adminView = adminView;
		this.restaurant = restaurant;
		
		this.adminView.addMenuItem(new addMenuListener(this.adminView, this.restaurant));
		this.adminView.deleteMenuItem(new removeMenuListener(this.adminView, this.restaurant));
		this.adminView.editMenuItem(new editMenuListener(this.adminView, this.restaurant));
		this.adminView.viewMenu(new viewMenuListener(this.adminView, this.restaurant));

	}
	
	private MenuItem newMenu(String menuName, Double menuPrice)
	{
		MenuItem menuProduct = null;
		List<String> menuComponent = this.adminView.getMenuComponent();
		if(menuComponent.isEmpty() && menuPrice != null)
		{
			menuProduct = new BaseProduct(menuName, menuPrice);
		}
		else if(!menuComponent.isEmpty() && menuPrice == null)
		{
			
			List<MenuItem> menu = this.restaurant.viewMenu();
			Set<BaseProduct> baseMenu = new HashSet<BaseProduct>();
			for(String componentName : menuComponent)
			{
				Boolean isInMenu = false;
				for(MenuItem menuItem : menu)
				{
					if(menuItem.getName().equalsIgnoreCase(componentName) && menuItem.getClass().getSimpleName().equals("BaseProduct"))
					{
						isInMenu = true;
						baseMenu.add((BaseProduct) menuItem);
						break;
					}
				}
				
				if(!isInMenu)
				{
					this.adminView.displayMessage("Item " + componentName + " is not in menu");
					return null;
				}
			}
			menuProduct = new CompositeMenu(menuName, baseMenu);
		}
		else
		{
			this.adminView.displayMessage("Invalid menu data or specification");
		}
		
		return menuProduct;
	}
	
	class addMenuListener implements ActionListener
	{
		private AdminView adminView;
		private Restaurant restaurant;
		
		public addMenuListener(AdminView adminView, Restaurant restaurant)
		{
			this.adminView = adminView;
			this.restaurant = restaurant;
		}
		
		public void actionPerformed(ActionEvent e)
		{
			String menuName = this.adminView.getMenuName();
			Double menuPrice = this.adminView.getMenuPrice();
			
			if(!menuName.isEmpty())
			{
				MenuItem menuProduct = newMenu(menuName, menuPrice);
				try
				{			
					if(menuProduct != null)
					{
						this.restaurant.addMenuItem(menuProduct);
						this.adminView.displayMessage("Item successfully added");
					}
					
				}
				catch(Exception ex)
				{
					this.adminView.displayMessage("Item is already in the menu in the specified form");
				}
			}
			else
			{
				this.adminView.displayMessage("Name for product not specified");
			}
		}
	}
	
	class removeMenuListener implements ActionListener
	{
		private AdminView adminView;
		private Restaurant restaurant;
		
		public removeMenuListener(AdminView adminView, Restaurant restaurant)
		{
			this.adminView = adminView;
			this.restaurant = restaurant;
		}
		
		public void actionPerformed(ActionEvent e)
		{
			String menuName = this.adminView.getMenuName();
			Double menuPrice = this.adminView.getMenuPrice();
			
			if(!menuName.isEmpty())
			{
				MenuItem menuProduct = newMenu(menuName, menuPrice);
				if(this.restaurant.getMenu().contains(menuProduct))
				{
					this.restaurant.removeMenuItem(menuProduct);
					this.adminView.displayMessage("Item succesfully removed from menu");
				}
				else
				{
					this.adminView.displayMessage("Item is not in the menu in the specified form");

				}
			}
			else
			{
				this.adminView.displayMessage("Name not specified");

			}
		}
	}
	
	class editMenuListener implements ActionListener
	{
		private AdminView adminView;
		private Restaurant restaurant;
		private MenuItem savedMenu;
		
		public editMenuListener(AdminView adminView, Restaurant restaurant)
		{
			this.adminView = adminView;
			this.restaurant = restaurant;
		}
		
		public void actionPerformed(ActionEvent e)
		{
			String menuName = this.adminView.getMenuName();
			Double menuPrice = this.adminView.getMenuPrice();
			
			if(!menuName.isEmpty())
			{
				MenuItem menuProduct = newMenu(menuName, menuPrice);
				if(savedMenu == null)
				{
					if(this.restaurant.getMenu().contains(menuProduct))
					{
						savedMenu = menuProduct;
						this.adminView.displayMessage("Modify the item");

					}
					else
					{
						this.adminView.displayMessage("Item is not in the menu in the specified form");

					}
				}
				else
				{
					if(menuProduct.getClass().getName().equals(savedMenu.getClass().getName()))
					{
						this.restaurant.editMenuItem(savedMenu, menuProduct);
						this.adminView.displayMessage("Item updated");

					}
					else
					{
						this.adminView.displayMessage("Those products are not of the same type. Update failed");

					}
					savedMenu = null;
				}
			}
			else
			{
				this.adminView.displayMessage("Name or price not specified");

			}
		}
	}
	
	class viewMenuListener implements ActionListener
	{
		private AdminView adminView;
		private Restaurant restaurant;
		
		public viewMenuListener(AdminView adminView, Restaurant restaurant)
		{
			this.adminView = adminView;
			this.restaurant = restaurant;
		}
		
		public void actionPerformed(ActionEvent e)
		{
			JFrame f = new JFrame("Display menu");
			f.setTitle("Display menu");
			f.setLocation(300, 300);
			f.setSize(400, 250);
			f.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			
			String col[] = {"Name", "Price", "Ingredients"};
			DefaultTableModel tableModel = new DefaultTableModel(col, 0);
			
			MenuItem[] menu = new MenuItem[this.restaurant.getMenu().size()];
			this.restaurant.getMenu().toArray(menu);
			
			for(MenuItem menuItem : menu)
			{
				String[] data = menuItem.toString().split("-");
				tableModel.addRow(data);
			}
			
			JTable t = new JTable(tableModel);		
			JScrollPane sp = new JScrollPane(t);
			f.add(sp);
			f.setVisible(true);
		}
	}

}
