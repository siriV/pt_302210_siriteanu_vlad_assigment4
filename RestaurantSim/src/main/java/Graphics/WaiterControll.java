package Graphics;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Menu.MenuItem;
import Restaurant.Order;
import Restaurant.Restaurant;

public class WaiterControll {
	
	private WaiterView waiterView;
	private Restaurant restaurant;
	
	public WaiterControll(WaiterView waiterView, Restaurant restaurant , Observer ob)
	{
		this.waiterView = waiterView;
		this.restaurant = restaurant;
		
		this.waiterView.addNewOrderListener(new newOrderListener(this.waiterView, this.restaurant));
		this.waiterView.addProductListener(new addProductListener(this.waiterView, this.restaurant));
		this.waiterView.removeProductListener(new removeProductListener(this.waiterView, this.restaurant));
		this.waiterView.sendOrderListener(new sendOrderListener(this.waiterView, this.restaurant, ob));
		this.waiterView.computeBillListener(new computeBillListener(this.waiterView, this.restaurant));
		this.waiterView.viewOrderListener(new viewOrderListener(this.waiterView, this.restaurant));
	}
	
	class newOrderListener implements ActionListener
	{
		private WaiterView waiterView;
		private Restaurant restaurant;
		
		public newOrderListener(WaiterView waiterView, Restaurant restaurant)
		{
			this.waiterView = waiterView;
			this.restaurant = restaurant;
		}
		
		public void actionPerformed(ActionEvent e)
		{
			Integer orderId = this.waiterView.getOrderId();
			Integer orderTable = this.waiterView.getOrderTable();
			
			if(orderId != null && orderTable != null)
			{
				Order order = new Order(orderId, orderTable);
				
				this.restaurant.newOrder(order);
				this.waiterView.displayMessage("New order created");
				
			}
			else
			{
				this.waiterView.displayMessage("Invalid order id or table");
			}
			
		}
	}
	
	class addProductListener implements ActionListener
	{
		private WaiterView waiterView;
		private Restaurant restaurant;
		
		public addProductListener(WaiterView waiterView, Restaurant restaurant)
		{
			this.restaurant = restaurant;
			this.waiterView = waiterView;
		}
		
		private MenuItem productInMenu(String productName)
		{
			List<MenuItem> menu = this.restaurant.viewMenu();
			for(MenuItem menuProduct : menu)
			{
				if(productName.equalsIgnoreCase(menuProduct.getName()))
				{
					return menuProduct;
				}
			}
			
			return null;
		}
		
		public void actionPerformed(ActionEvent e)
		{
			Integer orderId = this.waiterView.getOrderId();
			Integer orderTable = this.waiterView.getOrderTable();
			String productName = this.waiterView.getProduct();
			
			if(orderId != null && orderTable != null && !productName.isEmpty())
			{
				Order order = new Order(orderId, orderTable);
				MenuItem menuProduct = productInMenu(productName);
				
				if(menuProduct != null && this.restaurant.getOrder().containsKey(order))
				{
					
					this.restaurant.addOrderProduct(order, menuProduct);
					this.waiterView.displayMessage("Product " + menuProduct.getName() + " inserted in order number " + order.getId());
				}
				else
				{
					this.waiterView.displayMessage("Product is not in the menu or order does not exists");
				}
			}
			else
			{
				this.waiterView.displayMessage("Invalid order id or table or product");
			}
		}
	}
	
	class removeProductListener implements ActionListener
	{
		private WaiterView waiterView;
		private Restaurant restaurant;
		
		public removeProductListener(WaiterView waiterView, Restaurant restaurant)
		{
			this.restaurant = restaurant;
			this.waiterView = waiterView;
		}
		
		private MenuItem productInMenu(String productName)
		{
			List<MenuItem> menu = this.restaurant.viewMenu();
			for(MenuItem menuProduct : menu)
			{
				if(productName.equalsIgnoreCase(menuProduct.getName()))
				{
					return menuProduct;
				}
			}
			
			return null;
		}
		
		public void actionPerformed(ActionEvent e)
		{
			Integer orderId = this.waiterView.getOrderId();
			Integer orderTable = this.waiterView.getOrderTable();
			String productName = this.waiterView.getProduct();
			
			if(orderId != null && orderTable != null && !productName.isEmpty())
			{
				Order order = new Order(orderId, orderTable);
				MenuItem menuProduct = productInMenu(productName);
				
				if(menuProduct != null && this.restaurant.getOrder().containsKey(order) && this.restaurant.getOrder().get(order).contains(menuProduct))
				{
					
					this.restaurant.removeOrderProduct(order, menuProduct);
					this.waiterView.displayMessage("Product " + menuProduct.getName() + " removed from order number " + order.getId());
				}
				else
				{
					this.waiterView.displayMessage("Product is not in the menu or in the order");
				}
			}
			else
			{
				this.waiterView.displayMessage("Invalid order id or table or product");
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	class sendOrderListener extends Observable implements ActionListener
	{
		private WaiterView waiterView;
		private Restaurant restaurant;
		private Observer ob;
		
		public sendOrderListener(WaiterView waiterView, Restaurant restaurant, Observer ob)
		{
			this.waiterView = waiterView;
			this.restaurant = restaurant;
			this.ob = ob;
		}
		
		public void actionPerformed(ActionEvent e)
		{
			Integer orderId = this.waiterView.getOrderId();
			Integer orderTable = this.waiterView.getOrderTable();
			
			if(orderId != null && orderTable != null)
			{
				Order order = new Order(orderId, orderTable);
				if(!this.restaurant.getOrder().get(order).isEmpty())
				{
					setChanged();
					ob.update(this, order);
					this.waiterView.displayMessage("Order has been sent to Chef");

				}
				else
				{
					this.waiterView.displayMessage("Order is empty");

				}
			}
			else
			{
				this.waiterView.displayMessage("Invalid order id or table");
			}
			
		}
	}
	
	class computeBillListener implements ActionListener
	{
		private WaiterView waiterView;
		private Restaurant restaurant;
		
		public computeBillListener(WaiterView waiterView, Restaurant restaurant)
		{
			this.waiterView = waiterView;
			this.restaurant = restaurant;
		}
		
		public void actionPerformed(ActionEvent e)
		{
			Integer orderId = this.waiterView.getOrderId();
			Integer orderTable = this.waiterView.getOrderTable();
			
			if(orderId != null && orderTable != null)
			{
				Order order = new Order(orderId, orderTable);
				
				if(this.restaurant.getOrder().containsKey(order))
				{
					this.restaurant.computeBill(order);
					this.waiterView.displayMessage("Bill computed for order number " + order.getId());
				}
				else
				{
					this.waiterView.displayMessage("No order exists with id " + order.getId());
				}
				
			}
			else
			{
				this.waiterView.displayMessage("Invalid order id or table");
			}
			
		}
	}
	
	class viewOrderListener implements ActionListener
	{
		private WaiterView waiterView;
		private Restaurant restaurant;
		
		public viewOrderListener(WaiterView waiterView, Restaurant restaurant)
		{
			this.waiterView = waiterView;
			this.restaurant = restaurant;
		}
		
		public void actionPerformed(ActionEvent e)
		{
			
			JFrame f = new JFrame("Display orders");
			f.setTitle("Display orders");
			f.setLocation(300, 300);
			f.setSize(600, 250);
			f.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			
			String col[] = {"Number", "Table", "Products", "Price"};
			DefaultTableModel tableModel = new DefaultTableModel(col, 0);
			
			Order[] orderInfo = new Order[this.restaurant.getOrder().keySet().size()];
			
			this.restaurant.getOrder().keySet().toArray(orderInfo);
			
			for(int i = 0; i < orderInfo.length; i++)
			{
				String[] dataInfo  = orderInfo[i].toString().split("-");
				if(!this.restaurant.getOrder().get(orderInfo[i]).isEmpty())
				{
					String[] data = new String[4];
					data[0] = dataInfo[0];
					data[1] = dataInfo[1];
					data[2] = "";
					MenuItem[] orderData = new MenuItem[this.restaurant.getOrder().get(orderInfo[i]).size()];
					this.restaurant.getOrder().get(orderInfo[i]).toArray(orderData);
					for(MenuItem s : this.restaurant.getOrder().get(orderInfo[i]))
					{
						data[2] += s.getName() + ", ";
					}
					data[3] = restaurant.computeBill(orderInfo[i]).toString();
					dataInfo = data;
					}
				tableModel.addRow(dataInfo);
			}
			
			JTable t = new JTable(tableModel);		
			JScrollPane sp = new JScrollPane(t);
			f.add(sp);
			f.setVisible(true);
		}
	}
	
}
