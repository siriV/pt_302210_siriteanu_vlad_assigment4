package Graphics;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
public class AdminView extends JFrame{
	
	private JButton addMenuItem = new JButton("Add new menu item");
	private JButton editMenuItem = new JButton("Edit menu item");
	private JButton deleteMenuItem = new JButton("Delete menu item");
	private JButton viewMenu = new JButton("View menu");
	
	private JLabel menuName = new JLabel("Menu name: ");
	private JLabel menuPrice = new JLabel("Menu price: ");
	private JLabel composedMenu = new JLabel("Menu items: ");
	private JLabel message = new JLabel("Info: ");
	
	private JTextField menuNameTextBox = new JTextField();
	private JTextField menuPriceTextBox = new JTextField();
	private JTextField composedMenuTextBox = new JTextField();
	private JTextField messageTextBox = new JTextField();
	
	public AdminView()
	{
		JPanel adminPanel = new JPanel();
		JPanel namePanel = new JPanel();
		JPanel pricePanel = new JPanel();
		JPanel componentPanel = new JPanel();
		JPanel messagePanel = new JPanel();
		JPanel operationPanel = new JPanel();
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Admin view");
		this.setLocation(750, 0);
		this.setSize(550, 350);
		this.setResizable(false);
		
		namePanel.setLayout(new BoxLayout(namePanel, BoxLayout.X_AXIS));
		namePanel.add(menuName);
		namePanel.add(Box.createRigidArea(new Dimension(10, 0)));
		namePanel.add(menuNameTextBox);
		
		pricePanel.setLayout(new BoxLayout(pricePanel, BoxLayout.X_AXIS));
		pricePanel.add(menuPrice);
		pricePanel.add(Box.createRigidArea(new Dimension(12, 0)));
		pricePanel.add(menuPriceTextBox);

		componentPanel.setLayout(new BoxLayout(componentPanel, BoxLayout.X_AXIS));
		componentPanel.add(composedMenu);
		componentPanel.add(Box.createRigidArea(new Dimension(10, 0)));
		componentPanel.add(composedMenuTextBox);
		
		messagePanel.setLayout(new BoxLayout(messagePanel, BoxLayout.X_AXIS));
		messagePanel.add(Box.createRigidArea(new Dimension(45, 0)));
		messagePanel.add(message);
		messagePanel.add(Box.createRigidArea(new Dimension(10, 0)));
		messagePanel.add(messageTextBox);
		messageTextBox.setEditable(false);
		
		operationPanel.setLayout(new BoxLayout(operationPanel, BoxLayout.X_AXIS));
		operationPanel.add(addMenuItem);
		operationPanel.add(Box.createRigidArea(new Dimension(5, 0)));
		operationPanel.add(editMenuItem);
		operationPanel.add(Box.createRigidArea(new Dimension(5, 0)));
		operationPanel.add(deleteMenuItem);
		operationPanel.add(Box.createRigidArea(new Dimension(5, 0)));
		operationPanel.add(viewMenu);
		operationPanel.add(Box.createRigidArea(new Dimension(5, 0)));
		
		adminPanel.setLayout(new BoxLayout(adminPanel, BoxLayout.Y_AXIS));
		adminPanel.add(Box.createRigidArea(new Dimension(0, 80)));
		adminPanel.add(namePanel);
		adminPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		adminPanel.add(pricePanel);
		adminPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		adminPanel.add(componentPanel);
		adminPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		adminPanel.add(messagePanel);
		adminPanel.add(Box.createRigidArea(new Dimension(0, 40)));
		adminPanel.add(operationPanel);

		this.add(adminPanel);
		
	}
	
	public String getMenuName()
	{
		return this.menuNameTextBox.getText();
	}
	
	public Double getMenuPrice()
	{
		try
		{
			return Double.parseDouble(this.menuPriceTextBox.getText());
		}
		catch(Exception ex)
		{
			return null;
		}
	}
	
	public ArrayList<String> getMenuComponent()
	{
		String[] component = this.composedMenuTextBox.getText().split("[ ,]");
		ArrayList<String> replace = new ArrayList<String>();
		replace.add("");
		ArrayList<String> result = new ArrayList<String>(Arrays.asList(component));
		result.removeAll(replace);
		return result;
	}
	
	public void displayMessage(String message)
	{
		this.messageTextBox.setText(message);
	}
	
	public void addMenuItem(ActionListener listen)
	{
		this.addMenuItem.addActionListener(listen);
	}
	
	public void deleteMenuItem(ActionListener listen)
	{
		this.deleteMenuItem.addActionListener(listen);
	}
	
	public void editMenuItem(ActionListener listen)
	{
		this.editMenuItem.addActionListener(listen);
	}
	
	public void viewMenu(ActionListener listen)
	{
		this.viewMenu.addActionListener(listen);
	}

}
