package PT2020.T4;

import java.awt.Frame;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import Data.RestaurantSerializator;
import Graphics.AdminControll;
import Graphics.AdminView;
import Graphics.ChefView;
import Graphics.WaiterControll;
import Graphics.WaiterView;
import Menu.BaseProduct;
import Menu.CompositeMenu;
import Menu.MenuItem;
import Restaurant.Order;
import Restaurant.Restaurant;

/**
 * Hello world!
 *
 */
class WindowEventHandler extends WindowAdapter
{
	private Restaurant restaurant;
	private RestaurantSerializator ser;
	
	public WindowEventHandler(Restaurant restaurant, RestaurantSerializator ser)
	{
		this.restaurant = restaurant;
		this.ser = ser;
	}
	
	public void windowClosing(WindowEvent e)
	{
		ser.serialize(restaurant);
	}
}

public class App 
{
	
    public static void main( String[] args )
    {
    	
    	Restaurant restaurant;
    	RestaurantSerializator ser = new RestaurantSerializator(args[0]);
    	restaurant = ser.deserialize();
    	
    	ChefView cView = new ChefView();
    	cView.setVisible(true);

    	
    	WaiterView wView = new WaiterView();
    	WaiterControll wControll = new WaiterControll(wView, restaurant, cView);
    	wView.setVisible(true);
    	
    	
    	AdminView aView = new AdminView();
    	AdminControll aControll = new AdminControll(aView, restaurant);
    	aView.setVisible(true);
    	
    	Window[] windows =Frame.getWindows();
    	for(Window window : windows)
    	{
    		window.addWindowListener(new WindowEventHandler(restaurant, ser));
    	}
    }
}
