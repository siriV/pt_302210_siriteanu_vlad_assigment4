package Data;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class FileWriter {
	
	private FileWriter()
	{
		
	}
	
	public static Boolean billGenerator(Double price, Integer orderTable)
	{
		Path file = Paths.get("Bill.txt");
		
		List<String> priceString = new ArrayList<String>();
		priceString.add("Bill for table " + orderTable.toString() + ": " + price.toString());
		try {
			Files.write(file, priceString, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
			return true;
		} 
		catch (IOException e) 
		{
			System.out.println(e.getMessage());
			return false;
		}
		
	}

}
