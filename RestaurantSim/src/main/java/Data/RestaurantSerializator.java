package Data;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import Menu.MenuItem;
import Restaurant.Order;
import Restaurant.Restaurant;

public class RestaurantSerializator {
	
	private static String ser;
	private static int count;
	
	public RestaurantSerializator(String fileName)
	{
		if(count == 0)
		{
			this.ser = fileName;
			count++;
		}
		
	}
	
	public void serialize(Restaurant obj)
	{
		try {
			FileOutputStream file = new FileOutputStream(ser);
			ObjectOutputStream out = new ObjectOutputStream(file);
			
			out.writeObject(obj);
			
			out.close();
			file.close();
		} 
		catch (Exception e) {} 
		
	}
	
	public Restaurant deserialize()
	{
		Restaurant restaurant = new Restaurant(new HashMap<Order, ArrayList<MenuItem>>(),  new HashSet<MenuItem>());
		try
		{
			FileInputStream file = new FileInputStream(ser);
			ObjectInputStream in = new ObjectInputStream(file);
			
			restaurant = (Restaurant) in.readObject();
			
			in.close();
			file.close();
			
		}
		catch(Exception ex) {}
		
		return restaurant;
	}
}
