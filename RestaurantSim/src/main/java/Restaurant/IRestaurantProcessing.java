package Restaurant;

import java.io.Serializable;
import java.util.ArrayList;

import java.util.List;

import Menu.MenuItem;

/**
 * 
 * Interface for restaurant operations
 *
 */

public interface IRestaurantProcessing{
	
	/**
	 * 
	 * @param product Produs de adaugat in meniu
	 * 
	 * @pre product != null
	 * @post menu.size = menu.size@pre + 1 
	 * @post @forall i : [0, size - 1] 
	 * 		(i != hashCode(product) ==&gt; menu(i) == menu(i)@pre) &&
	 * 		(i == hashCode(product) ==&gt; menu(i) == product)
	 * 
	 */
	public void addMenuItem(MenuItem product);
	
	/**
	 * 
	 * @param product Produsul de eliminat din meniu
	 * @pre product != null
	 * @pre menu.size() &gt; 0
	 * 
	 * @post k = 1 @forall i : [0, size - 1] @
	 * 		(!menu(i).contains(product) || menu(i).getClass().getName().equals("BaseProduct") ==&gt; menu(i) == menu(i)@pre) &&
	 * 		(menu(i).contains(product)@pre ==&gt; menu.remove(i) == false, k++)
	 * 
	 * @post menu.remove(product) == false
	 * @post menu.size == menu.size@pre - k
	 */
	public void removeMenuItem(MenuItem product);
	
	/**
	 * 
	 * @param oldProduct vechiul produs ce trebuie editat
	 * @param newProduct noul produs editat
	 * 
	 * @pre oldProduct != null && newProduct != null
	 * @pre menu.size() &gt; 0
	 * @pre oldProduct.getClass().equals(newProduct.getClass())
	 * @post @forall i : [0, size - 1] @
	 * 		((!menu(i).contains(oldProduct) && !menu(i).contains(newProduct)) || menu(i).getClass().getName().equals("BaseProduct") ==&gt; menu(i) == menu(i)@pre) &&
	 * 		(menu(i).contains(oldProduct)@pre ==&gt; menu.remove(i) == false) &&
	 * 		(menu(i).contains(newProduct) ==&gt; menu(i.hash()@post) == menu(i.hash()@pre)@pre + newProduct)
	 *
	 *@post menu.containts(oldProduct) == false
	 *@post menu.contains(newProduct) == true
	 */
	
	public void editMenuItem(MenuItem oldProduct, MenuItem newProduct);
	
	/**
	 * 
	 * @return Lista ce contine toate produsele din meniu
	 * @pre true
	 * @post @nochange
	 */
	
	public ArrayList<MenuItem> viewMenu();
	/**
	 * 
	 * @param order O noua comanda
	 * 
	 * @pre order != null
	 * @post this.order.size = 0
	 * @post @forall i : [0, size - 1] @
	 * 		(i != hashCode(order) ==&gt; this.order(i) == this.order(i)@pre) &&
	 * 		(i == hashCode(order) ==&gt; this.order(i) == null)
	 */
	public void newOrder(Order order);
	
	/**
	 * 
	 * @param order Comanda pentru care se adauga produsul
	 * @param product Produsul care se adauga in comanda
	 * 
	 * @pre order != null
	 * @pre product != null
	 * @pre product.getPrice()&gt;0
	 * @pre this.order.containsKey(order)
	 * 
	 * @post this.order(order).size = this.order(order).size@pre + 1
	 * @post @forall i : [0, size - 1] @
	 * 		(i != hashCode(order) ==&gt; this.order(i) == this.order(i)@pre) &&
	 * 		(i == hashCode(order) ==&gt; this.order(i) == this.order(i).add(product)@pre)
	 */
	public void addOrderProduct(Order order, MenuItem product);
	
	/**
	 * 
	 * @param order Comanda pentru care se elimina produsul
	 * @param product Produsul care se elimina din comanda
	 * 
	 * @pre order != null
	 * @pre product != null
	 * @pre this.order.size &gt; 0
	 * @pre this.order.containsKey(order)
	 * 
	 * @post this.order(order).size = this.order(order).size@pre - 1
	 *
	 * @post @forall i : [0, size - 1] @
	 * 		(i != hashCode(order) ==&gt; this.order(i) == this.order(i)@pre) &&
	 * 		(i == hashCode(order) ==&gt; this.order(i) == this.order(i).remove(product)@pre)
	 */
	
	public void removeOrderProduct(Order order, MenuItem product);
	
	/**
	 * 
	 * @return Lista de produse de pe fiecare comanda
	 * 
	 *@pre true
	 *@post @nochange
	 */
	
	public ArrayList<ArrayList<MenuItem>> viewOrder();
	
	/**
	 * 
	 * @param order Comanda pentru care se elimina produsul
	 * @return Double pretul comenzii
	 * 
	 * @pre true
	 * 
	 * @post @nochange
	 */
	public Double computeBill(Order order);

}
