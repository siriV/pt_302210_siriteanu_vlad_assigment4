package Restaurant;

import Menu.BaseProduct;
import Menu.CompositeMenu;
import Menu.MenuItem;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import Data.FileWriter;

/**
 * 
 * @invariant isWellFormed()
 *
 */

public class Restaurant implements IRestaurantProcessing, Serializable{
	
	private Map<Order, ArrayList<MenuItem>> order;
	private Set<MenuItem> menu;
	
	
	public Restaurant(HashMap<Order, ArrayList<MenuItem>> order, HashSet<MenuItem> menu) 
	{
		this.order = order;
		this.menu = menu;
	}
	

	public Map<Order, ArrayList<MenuItem>> getOrder() 
	{
		return this.order;
	}

	public Set<MenuItem> getMenu() 
	{
		return (HashSet<MenuItem>) this.menu;
	}
	
	public Boolean isWellFormed()
	{
		for(Order ord : order.keySet())
		{
			for(MenuItem item : order.get(ord))
			{
				if(item.getPrice() <= 0.0 || !menu.contains(item))
				{
					return false;
				}
				else if(item.getClass().getSimpleName().equals("CompositeMenu"))
				{
					CompositeMenu cItem = (CompositeMenu) item;
					
					Double price = 0.0;
					for(BaseProduct bp : cItem.getProduct())
					{
						if(!menu.contains(bp))
						{
							return false;
						}
						
						price += bp.getPrice();
					}
					
					if(!price.equals(cItem.getPrice()))
					{
						return false;
					}
				}
			}
		}
		
		for(MenuItem item : menu)
		{
			if(item.getPrice() <= 0.0)
			{
				return false;
			}
			else if(item.getClass().getSimpleName().equals("CompositeMenu"))
			{
				CompositeMenu cItem = (CompositeMenu) item;
				
				Double price = 0.0;
				for(BaseProduct bp : cItem.getProduct())
				{
					if(!menu.contains(bp))
					{
						return false;
					}
					
					price += bp.getPrice();
				}
				
				if(!price.equals(cItem.getPrice()))
				{
					return false;
				}
			}
		}
		
		return true;
	}


	public void addMenuItem(MenuItem product) throws IllegalArgumentException
	{
		assert product != null;
		assert product.getPrice() > 0.0;
		assert isWellFormed();
		
		int size = menu.size();
		if(menu.add(product) == false)
		{
			throw new IllegalArgumentException();
		}
		
		assert menu.size() == size + 1;
		assert isWellFormed();
	}
	
	public void removeMenuItem(MenuItem product)
	{
		assert product != null;
		assert menu.size() > 0;
		assert isWellFormed();
		assert menu.contains(product) == true;

		int k = 0;
		
		int size = menu.size();
		
		if(menu.contains(product))
		{
			k++;
			List<MenuItem> toRemove = new ArrayList<MenuItem>();
			toRemove.add(product);
			if(product.getClass().getSimpleName().equals("BaseProduct"))
			{
				for(MenuItem prod : menu)
				{
					if(prod.getClass().getSimpleName().equals("CompositeMenu"))
					{
						CompositeMenu pr = (CompositeMenu) prod;
						if(pr.getProduct().contains(product))
						{
							toRemove.add(prod);
							k++;
						}
					}
				}				
			}
			for(Order ord : this.order.keySet())
			{
				for(MenuItem item : toRemove)
				{
					this.removeOrderProduct(ord, item);
				}
			}
			menu.removeAll(toRemove);

		}
		
		

		assert menu.size() == size - k;
		assert isWellFormed();

	}
	
	public void editMenuItem(MenuItem oldProduct, MenuItem newProduct)
	{
		assert oldProduct != null && newProduct != null;
		assert newProduct.getPrice() > 0.0;
		assert menu.size() > 0;
		assert isWellFormed();
		assert oldProduct.getClass().getSimpleName().equals(newProduct.getClass().getSimpleName());
		
		if(!menu.contains(oldProduct))
		{
			return;
		}
		
 		if(oldProduct.getClass().getSimpleName().equals("BaseProduct"))
		{	
			for(MenuItem prod : menu)
			{
				if(prod.getClass().getSimpleName().equals("CompositeMenu"))
				{
					CompositeMenu pr = (CompositeMenu) prod;
					if(pr.getProduct().contains(oldProduct))
					{
						pr.getProduct().remove(oldProduct);
						pr.getProduct().add((BaseProduct) newProduct);
						
					}
				}
			}
		}

 		menu.add(newProduct);
		for(Order ord : this.order.keySet())
		{
			int copy = Collections.frequency(this.order.get(ord), oldProduct);
			for(int i = 0; i < copy; i++)
			{
				this.addOrderProduct(ord, newProduct);
			}
		}
		
		for(Order ord : this.order.keySet())
		{
			this.removeOrderProduct(ord, oldProduct);
			
		}
		
		menu.remove(oldProduct);
		
		assert isWellFormed();
 		assert !menu.contains(oldProduct);
		assert menu.contains(newProduct);
	}
	
	public ArrayList<MenuItem> viewMenu()
	{
		return new ArrayList<MenuItem>(menu);
	}
	
	public void newOrder(Order order)
	{
		assert order != null;
		assert isWellFormed();
		
		this.order.put(order, new ArrayList<MenuItem>());
		
		assert isWellFormed();
		assert this.order.get(order).size() == 0;
	}
	
	public void addOrderProduct(Order order, MenuItem product)
	{
		
		assert order != null && product != null;
		assert product.getPrice() > 0;
		assert this.order.containsKey(order);
		assert isWellFormed();

		int size = this.order.get(order).size();

		if(menu.contains(product))
		{
			ArrayList<MenuItem> prod = this.order.get(order);
			
			prod.add(product);
			this.order.replace(order, prod);
			
			assert this.order.get(order).equals(prod);
			assert this.order.get(order).size() == size + 1;
		}
		
		assert isWellFormed();
	}
	
	public void removeOrderProduct(Order order, MenuItem product)
	{
		
		assert order != null && product != null;
		assert this.order.size() > 0;
		assert this.order.containsKey(order);
		assert isWellFormed();
		
		int size = this.order.get(order).size();

		if(menu.contains(product))
		{
			List<MenuItem> prod = this.order.get(order);

			while(prod.remove(product))
			{

				this.order.replace(order, (ArrayList<MenuItem>) prod);
				assert this.order.get(order).size()== size - 1;
			}
			assert this.order.get(order).equals(prod);
		}
		
		assert isWellFormed();
	}
	
	public ArrayList<ArrayList<MenuItem>> viewOrder()
	{
		return new ArrayList<ArrayList<MenuItem>>(order.values());
	}
	
	public Double computeBill(Order order)
	{
		Double price = 0.0;
		for(MenuItem product : this.order.get(order))
		{
			price += product.getPrice();
		}
		
		FileWriter.billGenerator(price, order.getTable());
		return price;
	}

}
