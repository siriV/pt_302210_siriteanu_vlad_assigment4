package Restaurant;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import Menu.MenuItem;

public class Order implements Serializable{

	private Integer orderId;
	private Integer table;
	
	public Order(Integer orderId, Integer table)
	{
		this.orderId = orderId;
		this.table = table;
	}
	
	public Integer getId()
	{
		return this.orderId;
	}
	
	public Integer getTable()
	{
		return this.table;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		result = prime * result + ((table == null) ? 0 : table.hashCode());
		return result;
	}

	public boolean equals(Object obj) 
	{
		if(obj == null)
		{
			return false;
		}
		
		Order ord = (Order) obj;
		if(!this.orderId.equals(ord.getId()))
		{
			return false;
		}
		else if(!this.table.equals(ord.getTable()))
		{
			return false;
		}
		return true;
	}

	
	public String toString() {
		return orderId.toString() + "-" + table.toString() + "-";
	}

	
	
}
